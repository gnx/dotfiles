set clipboard+=unnamedplus

set termguicolors
set guifont=Noto\ Sans\ Mono:h15
set guioptions=mlrb
colorscheme ayu

" кирилица
set keymap=bulgarian-phonetic
set iminsert=0 imsearch=-1
set imcmdline

" search
set incsearch
set ignorecase
set smartcase

" editor settings
set history=1000
set foldenable
set mouse=a
set number

" Default Indentation
set autoindent
set smartindent     " indent when
set tabstop=2       " tab width
set softtabstop=2   " backspace
set shiftwidth=2    " indent width
set textwidth=120
set smarttab
set expandtab       " expand tab to space
